package no.uib.inf101.sample.Model;

import java.util.ArrayList;
import java.util.List;

public class PersonList {

    private List<Person> persons = new ArrayList<>();

    
    public Iterable<Person> getPersons(){
        return this.persons;
    }

    public void addPerson(Person person) {
        this.persons.add(person);
    }
}
