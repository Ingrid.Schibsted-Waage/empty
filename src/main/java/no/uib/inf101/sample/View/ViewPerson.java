package no.uib.inf101.sample.View;

import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import no.uib.inf101.sample.Model.Person;

public class ViewPerson {

public static void main(String[] args){
    Person adam = new Person("Adam", 20);
    ViewPerson view = new ViewPerson(adam);

    JFrame frame = new JFrame("MOCK");
    frame.setContentPane(view.mainPanel);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);

}
    private JPanel mainPanel;
    
    public ViewPerson(Person p) {
        this.mainPanel = new JPanel();
        this.mainPanel.setLayout(new BoxLayout(mainPanel,BoxLayout.PAGE_AXIS));
        this.mainPanel.setPreferredSize(new Dimension(400,600));
        this.mainPanel.setBorder(new EmptyBorder(10,10,10,10));

        JLabel nameJLabel = new JLabel("Name: " + p.name());
        JLabel ageLabel = new JLabel("Age: " + p.age());
        
        
        this.mainPanel.add(nameJLabel);
        this.mainPanel.add(ageLabel);

    }
    public component getMainPanel{
        return null;
    }
}
