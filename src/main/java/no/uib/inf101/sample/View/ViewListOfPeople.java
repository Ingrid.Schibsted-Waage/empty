package no.uib.inf101.sample.View;

import java.awt.Component;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;

import no.uib.inf101.sample.Model.Person;
import no.uib.inf101.sample.Model.PersonList;

public class ViewListOfPeople {

    public static void main (String[] args){
        PersonList model = new PersonList();
        for (int i = 0; i < 10; i++){
            model.addPerson(new Person("Adam", 20));
            model.addPerson(new Person("Eva", 21));
        
        }

        ViewListOfPeople view = new ViewListOfPeople(model);
        
        JFrame frame = new JFrame("MOCK");
        frame.setContentPane(view.mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }

    private PersonList model;
    private JPanel mainPanel;
    public Component getMainpanel;

    public ViewListOfPeople(PersonList persons){
        this.model = model;
        this.mainPanel = new JPanel();

        //Make a list of persons and add them to the mainPanel
        DefaultListModel<Person> listModel = new DefaultListModel<>();
        JList<Person> list = new JList<>(listModel);
        for (Person person : model.getPersons()){
            listModel.addElement(person);
        }

        this.mainPanel.add(list);
    }
}
