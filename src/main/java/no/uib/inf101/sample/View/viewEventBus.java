package no.uib.inf101.sample.View;

import java.beans.EventHandler;
import java.util.logging.Handler;

public class viewEventBus {

    public void registerEventHandler(EventHandler eventHandler){
        
    }

    public void post(Event event){
        for (EventHandler handler : handlers){
            handler.handel(event);
        }
    }
    
}
