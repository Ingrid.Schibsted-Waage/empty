package no.uib.inf101.sample.View;

import javax.swing.JPanel;

import no.uib.inf101.sample.Model.PersonList;

public class ViewMain {

    private PersonList model;
    private JPanel mainPanel;
    private ViewListOfPeople viewListOfPeople;
    private ViewPerson viewPerson;
    
    public ViewMain(PersonList model){
        this.model = model;

        this.mainPanel = new JPanel();
        this.viewListOfPeople = new ViewListOfPeople(model);
        this.viewPerson = new ViewPerson(null);

        this.rightPanel = new JPanel();
        this.mainPanel.add(this.viewListOfPeople.getMainpanel());
        this.mainPanel.add(this.viewPerson.getMainPanel());

        this.rightPanel.add(viewPerson.getMainPanel())

    }
}
